class Potepan::Suggest < ActiveRecord::Base
  scope :search_by_keyword, -> (keyword) { where("keyword like :keyword", keyword: "#{keyword}%") if keyword.present? }
end
