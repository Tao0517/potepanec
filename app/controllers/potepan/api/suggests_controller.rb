class Potepan::Api::SuggestsController < Potepan::ApiController
  before_action :authenticate

  def index
    max_num = params[:max_num]
    if params[:keyword]
      suggests = Potepan::Suggest.search_by_keyword(params[:keyword])
      suggests = suggests.take(max_num) if max_num.to_i.positive?
      render json: suggests.pluck(:keyword)
    else
      render status: :unprocessable_entity, json: { status: 422, message: "keywordを設定してください" }
    end
  end
end
