require 'httpclient'

class Potepan::SuggestsController < ApplicationController
  def index
    url = Rails.application.credentials.suggest_api[:api_url]
    key = Rails.application.credentials.suggest_api[:api_key]
    client = HTTPClient.new
    query = { 'keyword' => params[:keyword], 'max_num' => params[:max_num] }
    header = { Authorization: "Bearer #{key}" }
    res = client.get(url, query, header)
    render json: res.body, status: res.status_code
  end
end
