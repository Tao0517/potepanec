require 'active_support/security_utils'

class Potepan::ApiController < ApplicationController
  include ActionController::HttpAuthentication::Token::ControllerMethods

  protected

  def authenticate
    render_unauthorized unless authenticate_token
  end

  def authenticate_token
    authenticate_with_http_token do |bearer, options|
      my_api_key = Rails.application.credentials.suggest_api[:api_key]
      ActiveSupport::SecurityUtils.secure_compare(bearer, my_api_key)
    end
  end

  def render_unauthorized
    render json: { message: 'Bearer invalid' }, status: :unauthorized
  end
end
