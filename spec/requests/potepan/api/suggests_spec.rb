require 'rails_helper'

RSpec.describe "Potepan::Api::Suggests", type: :request do
  let!(:keyword1) { create(:suggest, keyword: 'test1') }
  let!(:keyword2) { create(:suggest, keyword: 'test2') }
  let!(:keyword3) { create(:suggest, keyword: 'test3') }
  let!(:keyword4) { create(:suggest, keyword: 'test4') }
  let!(:keyword5) { create(:suggest, keyword: 'test5') }
  let!(:keyword6) { create(:suggest, keyword: 'test6') }
  let(:params) { { keyword: "t", max_num: 5 } }
  let(:params2) { { keyword: "t", max_num: "a" } }
  let(:params3) { { keyword: "t", max_num: 0 } }
  let(:params4) { { keyword: "t", max_num: nil } }
  let(:params5) { { max_num: 5 } }
  let(:headers) { { "Authorization" => "Bearer #{Rails.application.credentials.suggest_api[:api_key]}" } }

  describe "GET /potepan/api/suggests" do
    context "リクエストに成功したとき" do
      before do
        get potepan_api_suggests_path, params: params, headers: headers
      end

      it "正常にレスポンスを返すこと" do
        expect(response).to be_successful
      end

      it "検索候補をmax_numの数だけ取得できること" do
        json = JSON.parse(response.body)
        expect(json).to eq ["test1", "test2", "test3", "test4", "test5"]
      end
    end

    context "リクエストに成功したとき(max_numが数値以外)" do
      before do
        get potepan_api_suggests_path, params: params2, headers: headers
      end

      it "正常にレスポンスを返すこと" do
        expect(response).to be_successful
      end

      it "検索候補を全件取得すること" do
        json = JSON.parse(response.body)
        expect(json).to eq ["test1", "test2", "test3", "test4", "test5", "test6"]
      end
    end

    context "リクエストに成功したとき(max_numが0)" do
      before do
        get potepan_api_suggests_path, params: params3, headers: headers
      end

      it "正常にレスポンスを返すこと" do
        expect(response).to be_successful
      end

      it "検索候補を全件取得すること" do
        json = JSON.parse(response.body)
        expect(json).to eq ["test1", "test2", "test3", "test4", "test5", "test6"]
      end
    end

    context "リクエストに成功したとき(max_numがnil)" do
      before do
        get potepan_api_suggests_path, params: params4, headers: headers
      end

      it "正常にレスポンスを返すこと" do
        expect(response).to be_successful
      end

      it "検索候補を全件取得すること" do
        json = JSON.parse(response.body)
        expect(json).to eq ["test1", "test2", "test3", "test4", "test5", "test6"]
      end
    end

    context "キーワードパラメータを設定していないとき" do
      before do
        get potepan_api_suggests_path, params: params5, headers: headers
      end

      it "正常にレスポンスを返すこと(422error)" do
        expect(response).to have_http_status 422
      end
    end

    context "トークン認証に失敗したとき" do
      before do
        get potepan_api_suggests_path, params: params, headers: { "Authorization" => "Bearer invalid" }
      end

      it "正常にレスポンスを返すこと(401error)" do
        expect(response).to have_http_status 401
      end
    end
  end
end
