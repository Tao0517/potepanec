require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_products" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:related_product) { create(:product, taxons: [taxon]) }

    it "関連商品を持つこと" do
      expect(product.related_products).to include related_product
    end

    it "主商品を関連商品に表示しないこと" do
      expect(product.related_products).not_to include product
    end

    it "関連商品が重複しないこと" do
      expect(product.related_products).to eq product.related_products.distinct
    end
  end
end
