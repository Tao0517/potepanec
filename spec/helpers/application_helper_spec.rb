require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_titleヘルパーメソッドをテストすること" do
    before do
      @title = "BIGBAG Store"
    end

    context "page_titleが存在するとき" do
      it "pege_titleとともにbase_titleを返すこと" do
        expect(helper.full_title("Potepan")).to eq "Potepan - #{@title}"
      end
    end

    context "page_titleが空のとき" do
      it "base_titleのみを返すこと" do
        expect(helper.full_title("")).to eq @title
        expect(helper.full_title(nil)).to eq @title
      end
    end
  end
end
