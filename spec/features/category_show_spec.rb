require 'rails_helper'

RSpec.feature "CategoryShows", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "商品カテゴリーページが正しく動作すること" do
    within ".pageHeader" do
      within ".page-title" do
        expect(page).to have_content taxon.name
      end

      within ".active" do
        expect(page).to have_content taxon.name
      end

      within ".breadcrumb" do
        expect(page).to have_link "Home", href: potepan_path
      end
    end

    within ".sideBar" do
      click_link taxonomy.name
      expect(page).to have_content taxon.name
      expect(page).to have_content taxon.all_products.count
      click_link taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    within ".productBox" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
