require 'rails_helper'

RSpec.feature "ProductShows", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "商品詳細ページが正しく動くこと" do
    within ".pageHeader" do
      within ".page-title" do
        expect(page).to have_content product.name
      end

      within ".active" do
        expect(page).to have_content product.name
      end

      within ".breadcrumb" do
        expect(page).to have_link "Home", href: potepan_path
      end
    end

    within ".media-body" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
      expect(page).to have_link "一覧ページへ戻る", href: potepan_category_path(product.taxons.first.id)
    end

    within ".productsContent" do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
      expect(page).to have_selector ".productBox", count: 4
    end
  end
end
