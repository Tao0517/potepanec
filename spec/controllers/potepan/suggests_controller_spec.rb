require 'rails_helper'

RSpec.describe Potepan::SuggestsController, type: :controller do
  describe "GET #index" do
    let(:url) { Rails.application.credentials.suggest_api[:api_url] }
    let(:key) { Rails.application.credentials.suggest_api[:api_key] }

    context "status_codeが200のとき" do
      before do
        stub_request(:get, url).
          with(
            headers: { "Authorization" => "Bearer #{key}" },
            query: hash_including({ :keyword => "t", :max_num => "5" })
          ).to_return(
            status: 200,
            body: ["test1", "test2", "test3", "test4", "test5"].to_json,
            headers: { "Content-type" => "application/json" }
          )
        get :index, params: { keyword: "t", max_num: 5 }
      end

      it "正常にレスポンスを返すこと" do
        expect(response).to be_successful
      end

      it "res.bodyが取得できていること" do
        json = JSON.parse(response.body)
        expect(json).to eq ["test1", "test2", "test3", "test4", "test5"]
      end
    end

    context "status_codeがエラーのとき" do
      before do
        stub_request(:get, url).
          with(
            headers: { "Authorization" => "Bearer #{key}" },
            query: hash_including({ :keyword => "t", :max_num => "5" })
          ).to_return(
            status: 500,
            body: "error message"
          )
        get :index, params: { keyword: "t", max_num: 5 }
      end

      it "正常にレスポンスを返すこと(500error)" do
        expect(response).to have_http_status 500
      end

      it "エラーメッセージが返されること" do
        expect(response.body).to eq "error message"
      end
    end
  end
end
