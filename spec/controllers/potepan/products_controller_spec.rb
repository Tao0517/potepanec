require 'rails_helper'
require 'spree/testing_support/factories'
RSpec.describe Potepan::ProductsController, type: :controller do
  describe "product page" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end

    it "対応するビューが描画されていること" do
      expect(response).to render_template :show
    end

    it "productが適切であること" do
      expect(assigns(:product)).to eq product
    end

    it "related_productsが適切な数存在すること" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
