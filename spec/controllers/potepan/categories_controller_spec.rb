require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:products) { create(:product, taxons: [taxon]) }

  before do
    get :show, params: { id: taxon.id }
  end

  describe "category page" do
    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end

    it "対応するビューが描画されていること" do
      expect(response).to render_template :show
    end

    it "taxonが適切であること" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "taxonomiesが適切であること" do
      expect(assigns(:taxonomies).first).to eq taxonomy
    end

    it "productsが適切であること" do
      expect(assigns(:products).first).to eq products
    end
  end
end
